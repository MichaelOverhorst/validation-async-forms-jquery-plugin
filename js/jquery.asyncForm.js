(function($) {
    $.asyncForm = function(element, options) {
		var plugin = this;
		
        var defaults = {
			requiredFields: [],
			requiredClass: "validation-error",
            beforeSubmit: function() {},
            afterSubmit: function(success) {}
        }

        plugin.settings = {}
		
        plugin.init = function() {
            plugin.settings = $.extend({}, defaults, options);
        }

        plugin.init();
    }

    $.fn.asyncForm = function(options) {
        return this.each(function() {
			var $this = $(this);	
			var plugin = new $.asyncForm(this, options);
 
			$this.submit(function( event ) {
				//Trigger the "beforeSubmit" callback.
				plugin.settings.beforeSubmit($this);
				
				//Serialize the form.
				var formData = $this.serializeArray();
				var validationError = false;
				
				//Add checkbox support, which serializeArray() does not support this way.
				 var checkboxData = $('input:checkbox').map(function() {
				   return { name: this.name, value: this.checked ? this.value : "" };
				 });
				 
				 $.each(checkboxData, function(index, value) {
					formData.push({ name: value.name, value: value.value });
				 });
			
				//Form validation.
				$.each(formData, function(index, value) {
					var isRequiredField = $.inArray(value.name, plugin.settings.requiredFields) >= 0;
					var isEmptyField = !value.value;

					if(isRequiredField && isEmptyField){
						var $thisElement = $this.find("*[name='" + value.name + "']:first");

						//Rebind focus handler to remove the validation-error class on selection.
						$thisElement.unbind('focus').bind('focus', function(){
							$thisElement.removeClass(plugin.settings.requiredClass);
						});
						
						//Add validation error-class and set the validationError flag to true,
						//so the form will not submit current data.
						$thisElement.addClass(plugin.settings.requiredClass);
						validationError = true;
					}
				});
				
				if(!validationError){
					//Post the form async with ajax.
					$.ajax({
						cache: false,
						method: $this.prop('method'),
						url: $this.prop('action'),
						data: $.param(formData),
						//Trigger the "afterSubmit" with the status.
						success: function () {
							plugin.settings.afterSubmit(true); 
							$this[0].reset();
						},
						error: function () { 
							plugin.settings.afterSubmit(false); 
						}
					});					
				}
				
				//Disable the default post functionality of the form.
				event.preventDefault();
			});
        });
    }
})(jQuery);

